package patmat

import common._

/**
 * Huffman coding
 *
 */
object Huffman {

  /**
   * A huffman code is represented by a binary tree.
   *
   * Every `Leaf` node of the tree represents one character of the alphabet that the tree can encode.
   * The weight of a `Leaf` is the frequency of appearance of the character.
   *
   * The branches of the huffman tree, the `Fork` nodes, represent a set containing all the characters
   * present in the leaves below it. The weight of a `Fork` node is the sum of the weights of these
   * leaves.
   */
    abstract class CodeTree
  case class Fork(left: CodeTree, right: CodeTree, chars: List[Char], weight: Int) extends CodeTree
  case class Leaf(char: Char, weight: Int) extends CodeTree
  

  // Part 1: Basics
    	def weight(tree: CodeTree): Int = tree match {
		case Leaf(_, weight) => weight
		case Fork(_, _, _, weight) => weight

	}	

	def times(chars: List[Char]): List[(Char, Int)] = {
     def increment(accumulate:Map[Char, Int], c:Char) = {
       val count = (accumulate get c).getOrElse(0) + 1
       accumulate + ((c, count))
     }

     (Map[Char,Int]() /: chars)(increment).iterator.toList
   }

	def chars(tree: CodeTree): List[Char] = tree match {
		case Leaf(char, _) => List(char)
		case Fork(_, _, chars, _) => chars
	}

	def makeCodeTree(left: CodeTree, right: CodeTree) =
		Fork(left, right, chars(left) ::: chars(right), weight(left) + weight(right))
	
	def createCodeTree(chars: List[Char]): CodeTree =
		until(singleton, combine)(makeOrderedLeafList(times(chars)))

	
	def makeOrderedLeafList(freqs: List[(Char, Int)]): List[Leaf] =
		freqs.sortBy(_._2).map(p => Leaf(p._1, p._2))
	
	def makeOrderedLeafList(freqs: List[(Char, Int)]): List[Leaf] =
		freqs.sortBy(_._2).map( one => Leaf(one._1, one._2))
	
   
	def singleton(trees: List[CodeTree]): Boolean = trees.length == 1
	
	def decode(tree: CodeTree, bits: List[Bit]): List[Char] = {
		def traverse(remaining: CodeTree, bits: List[Bit]): List[Char] = remaining match {
		  case Leaf(c, _) if bits.isEmpty => List(c)
		  case Leaf(c, _) => c :: traverse(tree, bits)
		  case Fork(left, right, _, _) if bits.head == 0 => traverse(left, bits.tail)
		  case Fork(left, right, _, _) => traverse(right, bits.tail)
		}
    traverse(tree, bits)
	}
	
	def combine(trees: List[CodeTree]): List[CodeTree] = trees match {
		case Nil => trees
		case x :: Nil => trees
		case a :: b :: xs => (makeCodeTree(a, b) :: trees.tail.tail).sortBy(weight(_))
	  }
  
	def until(hd: List[CodeTree] => Boolean, t: List[CodeTree] => List[CodeTree])(trees: List[CodeTree]): CodeTree =
		if (hd(trees)) trees.head
		else until(hd, t)(t(trees))
	
    def encode(tree: CodeTree)(text: List[Char]): List[Bit] = {
		def encodeChar(tree: CodeTree)(char: Char): List[Bit] = tree match {
		  case Leaf(_, _) => List()
		  case Fork(left, right, _, _) => if (chars(left).contains(text.head)) 0 :: encodeChar(left)(char)
														   else 1 :: encodeChar(right)(char)
		}
    text flatMap(encodeChar(tree))
  }
  
	def quickEncode(tree: CodeTree)(text: List[Char]): List[Bit] = 
		text flatMap(codeBits(convert(tree)))
  
   type CodeTable = List[(Char, List[Bit])]

  /**
   returning bit sequence
   */
	def codeBits(table: CodeTable)(char: Char): List[Bit] = 
		table(table.indexWhere(x => x._1 == char))._2

	def convert(tree: CodeTree): CodeTable = tree match {
		case Leaf(char, _) => List((char, List()))
		case Fork(left, right, chars, _) => mergeCodeTables(convert(left), convert(right))
	}

	def mergeCodeTables(a: CodeTable, b: CodeTable): CodeTable = 
     a.map(code => (code._1, 0:: code._2)) ::: b.map(code => (code._1, 1 :: code._2))
	
	
  }
